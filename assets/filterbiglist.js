function filterBigList(qid){
    $("<label>Filtrer par : <input type='search' data-filterqid='"+qid+"' class='search filter' placeholder='Filtre'/></label>").insertBefore($("#question"+qid+" .answers-list"));
    $("#question"+qid+" .answers-list").addClass('list-filter');
    
    $("<label>Filtrer par : <input type='search' data-filterqid='"+qid+"' class='search filter' placeholder='Filtre'/></label>").insertBefore($("#question"+qid+" .subquestions-list"));
    $("#question"+qid+" .subquestions-list").addClass('list-filter');
}
$(document).on("keyup blur","input.search.filter",function(){
    var qid=$(this).data("filterqid");
    var search=$(this).val().trim().toUpperCase();
    if(search=="")
    {
        $("#question"+qid+" .answer-item").removeClass('filtered');
    }
    else
    {
        $( "#question"+qid+" .answer-item").each(function(){
            var label=$(this).find('label').text().toUpperCase();
            if(label.indexOf(search) >= 0)
            {
                $(this).removeClass('filtered');
            }
            else
            {
                $(this).addClass('filtered');
            }
        });
        $( "#question"+qid+" .answer-item.noanswer-item").removeClass('filtered');
        $( "#question"+qid+" .answer-item.other-item").removeClass('filtered');
        $( "#question"+qid+" .answer-item input:radio").each(function(){
            if($.inArray( $(this).attr('value'), biglistsetting.alwaysshow )>-1)
                $(this).parent('.answer-item').removeClass('filtered');
        });

    }
});
