$(function() {
    $(".list-radio").each(function(){
        if($(this).find(".answers-list .answer-item").length>biglistsetting.startat){
            filterBigList($(this).attr('id').substring(8));
        }
    });
    $(".multiple-opt").each(function(){
        if($(this).find(".subquestions-list .answer-item").length>biglistsetting.startat){
            filterBigList($(this).attr('id').substring(8));
        }
    });
    $(".dropdown-item select").each(function(){
        if($(this).find("option").length>biglistsetting.startat){
            var placeholder="";
            if($(this).find("option").first().val()=="")
            {
                var placeholder=$(this).find("option").first().text();
                $(this).find("option").first().text(""); // " " to have a select
            }
            $(this).combobox({
                select: function( event, ui ) { 
                    $(this).trigger('change'); 
                    
                    },
                create: function( event, ui ) {
                    ui.input.attr("placeholder", placeholder);
                    }
            });
        }
    });
});

function filterBigList(qid){
    $("<label>Filtrer par : <input type='search' data-filterqid='"+qid+"' class='search filter' placeholder='Filtre'/></label>").insertBefore($("#question"+qid+" .answers-list"));
    $("#question"+qid+" .answers-list").addClass('list-filter');
    
    $("<label>Filtrer par : <input type='search' data-filterqid='"+qid+"' class='search filter' placeholder='Filtre'/></label>").insertBefore($("#question"+qid+" .subquestions-list"));
    $("#question"+qid+" .subquestions-list").addClass('list-filter');
}
$(document).on("keyup blur","input.search.filter",function(){
    var qid=$(this).data("filterqid");
    var search=$(this).val().trim().toUpperCase();
    if(search=="")
    {
        $("#question"+qid+" .answer-item").removeClass('filtered');
    }
    else
    {
        $( "#question"+qid+" .answer-item").each(function(){
            var label=$(this).find('label').text().toUpperCase();
            if(label.indexOf(search) >= 0)
            {
                $(this).removeClass('filtered');
            }
            else
            {
                $(this).addClass('filtered');
            }
        });
        $( "#question"+qid+" .answer-item.noanswer-item").removeClass('filtered');
        $( "#question"+qid+" .answer-item.other-item").removeClass('filtered');
        $( "#question"+qid+" .answer-item input:radio").each(function(){
            if($.inArray( $(this).attr('value'), biglistsetting.alwaysshow )>-1)
                $(this).parent('.answer-item').removeClass('filtered');
        });

    }
});
$(document).on("click",".ui-combobox-button",function(){
    $(this).prev(".ui-combobox-input").val("");
});
$(document).on("blur",".ui-combobox-input",function(){
    $(this).val($(this).closest('.dropdown-item').find('option:selected').text());
});
