<?php
/**
 * filtreBigList Plugin for LimeSurvey
 * Allow to use a filter on some answers list
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2014 Denis Chenu <http://sondages.pro>
 * @license AGPL v3
 * @version 0.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
#use ls\pluginmanager\PluginBase;
class filterBigListSurvey extends PluginBase {
  protected $storage = 'DbStorage';
  static protected $description = 'Allow to filter some big list.';
  static protected $name = 'filter Big List by survey 0.1';

  protected $settings = array(
    'activefor'=>array(
      'type'=>'int',
      'label'=>'Activate the filter if there are more than X answers (O to allways deactivate).',
      'default'=>100,
    ),
  );

  public function __construct(PluginManager $manager, $id) {
    parent::__construct($manager, $id);
    $this->subscribe('beforeSurveySettings');
    $this->subscribe('newSurveySettings');
    $this->subscribe('beforeSurveyPage');

    //~ $this->subscribe('beforeQuestionRender','questionFilterBigList');
  }


  /**
   * This event is fired by the administration panel to gather extra settings
   * available for a survey.
   * The plugin should return setting meta data.
   * @param PluginEvent $event
   */
  public function beforeSurveySettings() {
    $oEvent = $this->event;
    $oEvent->set("surveysettings.{$this->id}", array(
      'name' => get_class($this),
      'settings' => array(
        'activefor'=>array(
          'type'=>'int',
          'label'=>'Activate the filter if there are more than X answers (O to allways deactivate).',
          'current' => $this->get('activefor', 'Survey', $oEvent->get('survey'),$this->get('activefor',null,null,$this->settings['activefor']['default'])),
        ),
        'alwaysshow'=>array(
          'type'=>'string',
          'label'=>'Allways show answer with code (only for single choice radio)',
          'current' => $this->get('alwaysshow', 'Survey', $oEvent->get('survey')),
        ),
      )
    ));
  }

  public function newSurveySettings()
  {
        $oEvent = $this->event;
        foreach ($oEvent->get('settings') as $name => $value)
        {
            if($name=='alwaysshow')
                $this->set($name, self::filterAlwaysShown($value), 'Survey', $oEvent->get('survey'));
            else
            {
                $default=$oEvent->get($name,null,null,isset($this->settings[$name]['default'])?$this->settings[$name]['default']:null);
                $this->set($name, $value, 'Survey', $oEvent->get('survey'),$default);
            }
        }
  }

    public function beforeSurveyPage()
    {
        $oEvent = $this->event;
        $aBiglistSettings=array(
            'startat' => $this->get('activefor', 'Survey', $oEvent->get('surveyId'),$this->get('activefor',null,null,$this->settings['activefor']['default'])),
            'alwaysshow' =>explode(',',self::filterAlwaysShown($this->get('alwaysshow', 'Survey', $oEvent->get('surveyId')))),
        );
        if($aBiglistSettings['startat'])
        {
            
            App()->getClientScript()->registerScript("aBiglistSettings","biglistsetting=".json_encode($aBiglistSettings),CClientScript::POS_BEGIN);
            $jsUrl = Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/filterbiglist_survey.js');
            App()->clientScript->registerScriptFile($jsUrl,CClientScript::POS_HEAD);
            $this->registerNeededFiles();
        }
    }
    public function questionFilterBigList()
    {
        $iActive=$this->get('activefor', 'Survey', $this->event->get('surveyId'),$this->get('activefor',null,null,$this->settings['activefor']['default']));
        if(!$iActive)
            return;
        $type=$this->event->get('type');
        if($type=="L")
        {
            $iQid=$this->event->get('qid');
            $sLang=Yii::app()->lang->langcode;
            $countAnswers= Answer::model()->count('qid=:qid and language=:language and scale_id=0',array(':qid'=>$iQid,':language'=>$sLang));
            if($countAnswers>=$iActive)
            {
                $this->registerNeededFiles();
                App()->getClientScript()->registerScript("fillterBigList{$iQid}","filterBigList({$iQid})",CClientScript::POS_END);

            }
        }
        if($type=="M")
        {
            $iQid=$this->event->get('qid');
            $sLang=Yii::app()->lang->langcode;
            $countQuestions= Question::model()->count('parent_qid=:qid and language=:language',array(':qid'=>$iQid,':language'=>$sLang));
            if($countQuestions>=$iActive)
            {
                $this->registerNeededFiles();
                App()->getClientScript()->registerScript("fillterBigList{$iQid}","filterBigList({$iQid})",CClientScript::POS_END);

            }
        }
    }
    private function registerNeededFiles()
    {
        static $done;
        if(!$done)// Think assets manager consule some time
        {
            // For ui.combobox : use directory for asset
            $comboboxUrl = Yii::app()->assetManager->publish(dirname(__FILE__) . '/third-party/jquery.ui.combobox');
            App()->clientScript->registerScriptFile($comboboxUrl."/jquery.ui.combobox.js",CClientScript::POS_HEAD);
            App()->clientScript->registerCssFile($comboboxUrl."/jquery.ui.combobox.css");
            // For own : unsure is updated when plugin is updated : each file for asset
            $jsUrl = Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/filterbiglist.js');
            App()->clientScript->registerScriptFile($jsUrl,CClientScript::POS_HEAD);
            $cssUrl = Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/filterbiglist.css');
            App()->clientScript->registerCssFile($cssUrl);


            $done=true;
        }
    }

    private static function filterAlwaysShown($string)
    {
        if(!empty($string) )
        {
            $aAlwaysShown=explode(",",$string);
            $aFixedAlwaysShown=array();
            foreach($aAlwaysShown as $sAlwayShow)
            {
                $sAlwayShow=preg_replace("/[^A-Za-z0-9]/", '', $sAlwayShow);
                if(!empty($sAlwayShow))
                    $aFixedAlwaysShown[]=$sAlwayShow;
            }
            return implode(',',$aFixedAlwaysShown);
        }
    }
}
